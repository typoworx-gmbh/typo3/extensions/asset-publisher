<?php
declare(strict_types=1);
namespace Interfrog\IfMainmetall\EventListener\AssetRenderer;

use TYPO3\CMS\Core\Page\Event\BeforeStylesheetsRenderingEvent;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Cache\Event\CacheFlushEvent;
use TYPO3\CMS\Core\Cache\Event\CacheWarmupEvent;
use TYPO3\CMS\Core\Page\Event\AbstractBeforeAssetRenderingEvent;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

final class MediaAssets
{
    private int $cacheTTL = 300;
    private bool $skipProcessing = false;
    private array $packageNamespace;
    private ?string $webAssetExtensionPath = null;
    private string $extensionResourcePublicPath;

    private ?array $ignoredPattern = [];
    private array $allowedFileTypes = [];

    private ?Registry $registry;
    private string $registryNamespace;

    protected null|AbstractBeforeAssetRenderingEvent|CacheFlushEvent|CacheWarmupEvent $event = null;


    public function __invoke(AbstractBeforeAssetRenderingEvent|CacheFlushEvent|CacheWarmupEvent $event): void
    {
        $this->event = $event;

        if ($event instanceof CacheFlushEvent)
        {
            $this->flush();
            $this->warmup();
        }
        else if ($event instanceof CacheWarmupEvent)
        {
            // @ToDo Garbage Collector!
            $this->flush();
            $this->warmup();
        }
        else if ($event instanceof AbstractBeforeAssetRenderingEvent)
        {
            // @ToDo Garbage Collector!
            $this->warmup();
        }
        else
        {
            throw new \InvalidArgumentException(sprintf('Received unknown Event: %s', get_class($event)));
        }
    }

    public function __construct(
        ?string $package = null,
        array $allowedExtensions = [],
        bool $allowDefaultGFX = true,
        array $ignoredPattern = null,
        int $cacheTTL = 300
    )
    {
        $this->registry = GeneralUtility::makeInstance(Registry::class);
        $this->registryNamespace = sprintf('%s::MediaAssets', $package);
        $this->cacheTTL = $cacheTTL;

        $this->ignoredPattern = $ignoredPattern;
        $this->prepare(
            package: $package,
            allowedExtensions: $allowedExtensions,
            allowDefaultGFX: $allowDefaultGFX,
        );
    }

    protected function skip() : bool
    {
        if ($this->skipProcessing === true)
        {
            return true;
        }

        $now = (new \DateTime());
        $this->skipProcessing = (bool)$this->registry->get($this->registryNamespace, 'is_cached', false);

        if ($this->skipProcessing === true)
        {
            if (!is_dir($this->webAssetExtensionPath))
            {
                return false;
            }

            $isGenerated = $this->registry->get($this->registryNamespace, 'is_generated', null);

            // Check if cache has invalidated
            if ($isGenerated instanceof \DateTime)
            {
                $isGenerated->modify(sprintf('+%d seconds', $this->cacheTTL ?? 300));

                if ($isGenerated < $now)
                {
                    $this->skipProcessing = false;
                    $this->registry->set($this->registryNamespace, 'is_cached', false);

                    return false;
                }
            }
        }

        $this->registry->get($this->registryNamespace, 'is_generated', $now);

        return $this->skipProcessing;
    }

    protected function flush() : void
    {
        $this->skipProcessing = false;
        $this->registry->set($this->registryNamespace, 'is_cached', false);
        $this->registry->set($this->registryNamespace, 'is_generated', null);
    }

    protected function warmup() : void
    {
        $this->providePublicResources();
    }

    private function prepare(?string $package, ?array $allowedExtensions, bool $allowDefaultGFX = true) : void
    {
        if ($this->webAssetExtensionPath !== null)
        {
            return;
        }

        [$vendor, $packageName] = explode('\\', $package ?? __NAMESPACE__, 3);
        $extensionName = GeneralUtility::camelCaseToLowerCaseUnderscored($packageName);
        $this->packageNamespace = [ 'vendor' => $vendor, 'package' => $extensionName ];

        if (count($allowedExtensions))
        {
            array_walk($allowedExtensions, function(&$v) { return trim($v, " .\t\n\r\0\x0B"); });
            $this->allowedFileTypes = $allowedExtensions;
        }
        $this->configureAllowedMediaTypes(allowDefaultGFX: $allowDefaultGFX);

        $webAssetsPath = sprintf('%s/_assets', Environment::getPublicPath());
        if (!is_dir($webAssetsPath) && !file_exists($webAssetsPath))
        {
            mkdir($webAssetsPath);
        }

        $this->webAssetExtensionPath = sprintf('%s/%s/%s', $webAssetsPath, strtolower($vendor), $extensionName);
    }

    protected function configureAllowedMediaTypes(bool $allowDefaultGFX = true)
    {
        if ($allowDefaultGFX === true)
        {
            $this->allowedFileTypes += explode(',', $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'] ?? []);
        }
    }

    protected function checkIgnoredPattern(\SplFileInfo $file) : bool
    {
        foreach($this->ignoredPattern as $ignorePattern)
        {
            if (preg_match($ignorePattern, $file->getPathname()))
            {
                return true;
            }
        }

        return false;
    }

    protected function providePublicResources() : void
    {
        if ($this->skip() === true)
        {
            return;
        }

        $this->skipProcessing = true;

        [, $package] = array_values($this->packageNamespace);
        $extensionKey = GeneralUtility::camelCaseToLowerCaseUnderscored($package);

        $extensionPath = ExtensionManagementUtility::extPath($extensionKey);
        $this->extensionResourcePublicPath = sprintf('%sResources/Public', $extensionPath);

        $publicFilesRecursiveIterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                $this->extensionResourcePublicPath,
                \FilesystemIterator::SKIP_DOTS & \FilesystemIterator::UNIX_PATHS
            ),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        /** @var \SplFileInfo $file */
        foreach($publicFilesRecursiveIterator as $file)
        {
            if ($file->getType() == 'dir')
            {
                continue;
            }

            if (str_starts_with($file->getBasename(), 'Extension.') && basename($file->getPath()) === 'Icons')
            {
                continue;
            }

            if ($this->checkIgnoredPattern($file))
            {
                continue;
            }

            self::createResourceLink(file: $file);
        }

        $this->registry->set($this->registryNamespace, 'is_cached', true);
    }

    private function createResourceLink(\SplFileInfo $file) : void
    {
        if (!$this->webAssetExtensionPath)
        {
            return;
        }

        $assetRelPath = PathUtility::getRelativePath($this->extensionResourcePublicPath, $file->getPath());
        $assetExtensionPath = sprintf('%s/%s', $this->webAssetExtensionPath, $assetRelPath);
        $assetFile = sprintf('%s/%s%s', $this->webAssetExtensionPath, $assetRelPath, $file->getBasename());

        if (is_link($assetFile) || file_exists($assetFile))
        {
            return;
        }

        if (is_dir($assetExtensionPath) || mkdir ($assetExtensionPath, 0777, true))
        {
            try
            {
                symlink($file->getPathname(), $assetFile);
            }
            catch (\Exception $e)
            {
                throw new Exception(
                    sprintf(
                        'Asset file cannot be linked: %s -> %s',
                        $file->getPathname(), $assetFile
                    ),
                    1703089127,
                    $e
                );
            }
        }
    }
}
