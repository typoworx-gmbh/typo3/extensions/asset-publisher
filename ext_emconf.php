<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Tnm Asset-Publisher',
    'description' => 'Provides an extension utility to link \'Resources/Public\' Assets unhashed into \'{public}/_assets/{vendor}/{extension}/\' to use them in CSS without Fluid-Assets',
    'category' => 'plugin',
    'author' => 'Gabriel Kaufmann',
    'author_email' => 'info@typoworx.com',
    'author_company' => 'TYPOworx GmbH',
    'state' => 'beta',
    'version' => '0.5',
    'constraints' => [
        'depends' => [
            'typo3' => '10.5-12.5'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'TYPOworx\TnmAssetPublisher\\' => 'Classes'
        ],
    ],
];
