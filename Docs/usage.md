# TYPOworx Media-Assets

## What does this extension do?
Since TYPO v12 Extensions Resources/Public is no longer available in web-public root, which is a great benefit at first glance.
Instead Fluid and AssetCollectors in TYPO3-Core are linking the extension's Resources/Public into ```web-public/_assets/{hash}```.

But there are some edge-cases where this makes many things complicated. If one needs to include f.e. js/css/fonts ressources from Extension's
without using fluid uri-viewHelpers or PHP-AssetCollectors, it's currently impossible to access these ressources from extensions.AssetCollectors.

This is where this extension comes in place. This extension is configureable using Services.yaml to configure and enable our custom MediaAsset-Linker,
which then will link extension public-ressources non-hashed into ```web-public/_assets/{vendor}/{extension-name}/```. All files underneath are linked
using symlinks and therefore it's possible to configure some filters on file-types and paths in Services.yaml to exclude specific directories and files
being linked 'insecure' into ```web-public/_assets/{vendor}/{extension-name}/```. So it's also possible to only link essentially needed ressources.

See Docs/Contrib/Services.yaml for further informations on how to configure it.
[./Docs/Contrib/Services.yaml](./Docs/Contrib/Services.yaml)
